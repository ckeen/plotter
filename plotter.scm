(module plotter

        (new-win
         win-ezx-handle
         simple-draw
         draw-simple-axes)

(import chicken scheme)
(use miscmacros srfi-1 ezxdisp)

(define-record win ezx-handle w h x-border y-border title)

(define black (make-ezx-color 0 0 0))
(define red (make-ezx-color 1 0 0))
(define green (make-ezx-color 0 1 0))

(define *grid-width* 2)
(define *circle* 1)

(define (new-win w h title) (make-win (ezx-init w h title) w h 0.05 0.10 title))

; remaps a value from a [source-low;source-high] to the
; [target-low;target-high] intervall
(define-inline (remap value source target)
  (let ((source-low (car source))
        (source-high (cadr source))
        (target-low (car target))
        (target-high (cadr target)))
    (round (+ target-low
              (* (- target-high target-low)
                 (/ (- value source-low)
                    (- source-high source-low)))))))

; walks the list once and returns its '(min max)
(define (min-max lst)
  (fold (lambda (c s)
          (list (min c (car s))
                (max c (cadr s))))
        (list +inf.0 -inf.0)
        lst))

; remaps a list of (x y) coordinates to the new range
(define (xy-remap lst x-range-target y-range-target)
  (let* ((xs (map car lst))
        (ys (map cadr lst))
        (x-src (min-max xs))
        (y-src (min-max ys)))
    (map (lambda (x y)
           (list (remap x x-src x-range-target)
                 (remap y y-src y-range-target)))
         xs ys)))

(define (win-range win)
  (let* ((h (win-h win))
         (w (win-w win))
         (x-offset (* w (win-x-border win)))
         (y-offset (* h (win-y-border win)))
         (x-range (list x-offset (- w (* 2 x-offset))))
         (y-range (list y-offset (- h (* 2 y-offset)))))
    (values x-range y-range)))

(define (norm-y-coords win y)
  (let ((y-offset (* (win-h win) (win-y-border win))))
    ;; the y coordinates need to be "swapped" since our coordinate system
    ;; starts in the upper left corner
    (- (win-h win) y-offset y)))

; draws a list containing '(x y) coordinates to a window using circles
(define (simple-draw win lst)
  (let-values
   (((x-range y-range) (win-range win)))
   (let ((vals (xy-remap lst x-range y-range)))
     (for-each (lambda (v)
                 (ezx-fillcircle-2d
                  (win-ezx-handle win)
                  (car v)
                  (norm-y-coords win (cadr v))
                  *circle*
                  black))
               vals)
  (ezx-redraw (win-ezx-handle win)))))

(define (get-axis-marks vals range selector #!optional (start 0))
  (let* ((val-range (min-max vals)))
    (filter (lambda (v) (<= (car range) (car v)))
            (map (lambda (v)
                   (list (remap v val-range range) v))
                 (let filter ((times (- (floor (cadr val-range)) start))
                              (x start)
                              (res '()))
                   (cond ((= 0 times) (reverse res))
                         (else (filter (sub1 times) (add1 x) (if (selector x)
                                                                   (cons x res)
                                                                   res)))))))))

(define (draw-simple-axes win orig x-filter x-printer y-filter y-printer)
  (let* ((xs (map car orig))
        (ys (map cadr orig))
        (x-src (min-max xs))
        (y-src (min-max ys)))
    (let-values
     (((x-range y-range) (win-range win)))
     ; y axis
     (ezx-line-2d
      (win-ezx-handle win)
      (car x-range)
      (norm-y-coords win (car y-range))
      (car x-range)
      (norm-y-coords win (cadr y-range))
      black
      *grid-width*)
     ; y axis marks
     (for-each
      (lambda (v)
        (ezx-line-2d
         (win-ezx-handle win)
         (car x-range)
         (norm-y-coords win (car v))
         (- (car x-range) 5)
         (norm-y-coords win (car v))
         black
         *grid-width*)
        (ezx-str-2d (win-ezx-handle win)
                    (- (car x-range)
                       (* (car x-range) 0.75))
                    (norm-y-coords win (car v))
                    (y-printer (cadr v))
                    black))
      (get-axis-marks
       (map cadr orig)
       y-range
       y-filter
       (car y-range)))
     ; x axis
     (ezx-line-2d
      (win-ezx-handle win)
      (car x-range)
      (norm-y-coords win (car y-range))
      (cadr x-range)
      (norm-y-coords win (car y-range))
      black
      *grid-width*)
     (for-each
      (lambda (v)
        (ezx-line-2d
         (win-ezx-handle win)
         (car v)
         (norm-y-coords win (car y-range))
         (car v)
         (norm-y-coords win (- (car y-range) 5))
         black
         *grid-width*)
        (ezx-str-2d (win-ezx-handle win)
                    (car v)
                    (norm-y-coords win (- (car y-range)
                                          (* (car y-range) 0.75)))
                    (x-printer (cadr v))
                    black))
      (get-axis-marks (map car orig)
                      x-range
                      x-filter
                      (car x-src)))
     (ezx-redraw (win-ezx-handle win))))))